//ALL ABOUT THAT ACTION BABY
export const DELETE_TODO = "DELETE_TODO"
export const TOGGLE_TODO = "TOGGLE_TODO"
export const ADD_TODO = "ADD_TODO"
export const CLEAR_COMPLETED_TODOS = "CLEAR_COMPLETED_TODOS"

//creatures of action (action creator)

export function deleteTodo (id){
    return {
        type: DELETE_TODO,
        id
    }
}

export function toggleTodo (todo) {
    return {
        type: TOGGLE_TODO,
        todo
    }
}

export function addTodo (input){
    return {
        type: ADD_TODO,
        input: input
    }
}

export function clearCompletedTodos () {
    return {
        type: CLEAR_COMPLETED_TODOS
    }
}