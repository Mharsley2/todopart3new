import React, { Component } from "react";
import "./index.css";
// import todosList from "./todos.json";
import { Route, NavLink } from "react-router-dom";
import TodoList from "./TodoList.jsx"
import { connect } from "react-redux"
// import { BrowserRouter as Router, Route, NavLink } from "react-router-dom";
import { clearCompletedTodos, addTodo } from "./action"


class App extends Component {
  state = {
    input: ""
  };
  //toggle todo complete
  // handleToggleClick = todo => event => {
  //   let tempState = this.state
  //   todo.completed = !todo.completed
  //   this.setState({ tempState })
  // }
  //
  // handleDestroyClick = todo => event => {
  //   let tempState = this.state
  //   let newTodos = tempState.todos.filter(
  //     item => item.id !== todo.id
  //   )
  //   this.setState({ todos: newTodos })
  // }

  //create functionality clear completeclick
  // handleClearCompleteClick = event => {
  //   let tempState = this.state
  //   let tempTodos = this.state.todos.filter(
  //     todo => todo.completed === false
  //   )
  //   tempState.todos = tempTodos
  //   this.setState({ tempState })
  // }
  //create todo(handle it)
  handleCreateTodo = event => {
    if (event.keyCode === 13) {
      this.props.addTodo(event.target.value)
        event.target.value = ''
      }
    }
      
  //handle the change!
  handleChange = event => {
    this.setState({ input: event.target.value })
  }
  render() {
    return (
        <section className="todoapp">
          <header className="header">
            <h1>todos</h1>
            <input
              className="new-todo"
              placeholder="What needs to be done?"
              autoFocus
              //was confused when terminal was like "error, autofocus what?" well this is why!
              value={this.state.input}
              onChange={this.handleChange}
              onKeyDown={this.handleCreateTodo}
            />
          </header>
          {/* lets get this routing done */}
          <Route exact path='/' render={() => (
            <TodoList todos={this.props.todos}
              handleToggleClick={this.handleToggleClick} />
          )} />
          <Route path='/active' render={() => (
            <TodoList todos={this.props.todos.filter(todo => {
              if(todo.completed === false ){
                return todo
              }
              return false
            })}
              handleToggleClick={this.handleToggleClick} />
          )} />
          <Route path='/completed' render={() => (
            <TodoList todos={this.props.todos.filter(todo => {
              if(todo.completed === true){
                return todo
              }
              return false
            }
            )}
              handleToggleClick={this.handleToggleClick} />
          )} />
          <footer className="footer">
            {/* <!-- This should be `0 items left` by default --> */}
            <span className="todo-count">
              <strong>{this.props.todos.filter(todo => {
              if(todo.completed === false ){
                return todo
              }
              return false
            }).length}</strong> item(s) left
          </span>
            <ul className="filters">
              <li>
                <NavLink exact to='/' activeClassName = 'selected'>All</NavLink>
              </li>
              <li>
                <NavLink to="/active" activeClassName = 'selected'>Active</NavLink>
              </li>
              <li>
                <NavLink to="/completed" activeClassName = 'selected'>Completed</NavLink>
              </li>
            </ul>
            {/* made sure that clear button still works! */}
            <button className="clear-completed" onClick={event => this.props.clearCompletedTodos()}>Clear completed</button>
          </footer>
        </section>
    );
  }
}

// put router tag at highest level to intercept any history changes and add new path
// for new page into browser history stack

function mapStateToProps(state){
  return {
    todos: state.todos
  }
}
const mapDispatchToProps = {
  clearCompletedTodos,
  addTodo
};


export default connect(mapStateToProps, mapDispatchToProps)(App);